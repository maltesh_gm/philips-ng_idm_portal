import { Component, PipeTransform, Pipe } from '@angular/core';
// import { MdDialog, MdDialogRef, MdToolbar, MdCard, MaterialModule } from '@angular/material';
import { DashboardService, DashboardInterface } from './dashboard.service';
import { KeysPipe } from './pipe';

@Component({
    selector: 'material-app',
    viewProviders: [DashboardService],
    templateUrl: 'app/templates/app.component.html',
    styleUrls: ['app/templates/app.component.css'],
})
export class AppComponent {
    
}
