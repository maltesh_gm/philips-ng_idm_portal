import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from '@angular/material';
import { HttpModule } from '@angular/http';
//Import Custom Modules
import { KeysPipe } from './pipe';
import { HostService } from './hostinfo.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardProductComponent } from './dashboard/dashboard.products';


@NgModule({
  imports: [BrowserModule, MaterialModule.forRoot()],
  declarations: [DashboardComponent, KeysPipe, DashboardProductComponent],
  bootstrap: [DashboardComponent],
  providers: [HostService]
})
export class AppModule { }
