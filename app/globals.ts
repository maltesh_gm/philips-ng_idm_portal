export const HOST_CONFIGURATION = {
    'ECOSYSTEM_URI':'127.0.0.1:8080/',
    // pma/161.85.30.207
    'ECOSYSTEM_MOUNT':''
}

export const SERVICE_END_POINTS = {
    'DASHBOARD': {
        'url': 'dashboard/stats'
    },
    'DB_PRODUCTS': {
        'url': 'dashboard/stats/products'
    },
    'DB_HOSTS_DOWN': {
        'url': 'dashboard/unreachableservices/count'
    }
}
