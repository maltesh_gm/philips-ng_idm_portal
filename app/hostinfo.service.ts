import { Injectable } from '@angular/core';
import myGlobals = require('./globals');

class HostInfo {
    private static _instance: HostInfo = new HostInfo();
    service: string;

    construct() {
        if (HostInfo._instance) {
            throw new Error("Error: Instantiation failed: Use HostInfo.getInstance() instead of new.");
        }
        HostInfo._instance = this;
    }

    public static getInstance(): HostInfo {
        return HostInfo._instance;
    }

    getSystemUri() {
        var hs_pl = document.location.protocol;
        var host = hs_pl + '//' + this.getHostName() + "/";
        //Host protocol-http or https
        if (!myGlobals.HOST_CONFIGURATION.ECOSYSTEM_MOUNT) {
            console.log(hs_pl + '//' + myGlobals.HOST_CONFIGURATION.ECOSYSTEM_URI);
            return hs_pl + '//' + myGlobals.HOST_CONFIGURATION.ECOSYSTEM_URI;
        } else {
            return hs_pl + '//' + myGlobals.HOST_CONFIGURATION.ECOSYSTEM_URI + myGlobals.HOST_CONFIGURATION.ECOSYSTEM_MOUNT;
        }
    }

    getHostName() {
        return document.location.hostname;
    }

    getServiceEndPoint(end_point: string) {
        this.service = end_point;
        if (myGlobals.SERVICE_END_POINTS[this.service]) {
            return this.getSystemUri() + myGlobals.SERVICE_END_POINTS[this.service]['url'];
        } else {
            console.error('Service end point not defined !! Failed to load service ' + this.service);
        }
    }
}

@Injectable()
export class HostService {
    service: string;
    url: string;
    hostInfo: HostInfo;
    construct() {
        this.hostInfo = HostInfo.getInstance();
    }

    getUrl(service: string) {
        this.hostInfo = HostInfo.getInstance();
        this.url = this.hostInfo.getServiceEndPoint(service);
        return this.url;
    }

    get(service: string) {
        let url = this.getUrl(service)
    }

}