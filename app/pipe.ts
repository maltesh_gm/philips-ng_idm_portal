import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {
    transform(value: String, args: string[]): any {
        let keys1 : {key: string, value: string }[] =[];
        for (let key in value) {
            keys1.push({ key: key, value: value[key] });
        }
        return keys1;
    }
}