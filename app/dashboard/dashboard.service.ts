import { Inject } from '@angular/core';
import { Http } from '@angular/http';
import { HostService } from '../hostinfo.service';
import { Headers} from '@angular/http';

export interface DashboardInterface {
    result: Array<any>;
}

export class DashboardService {
    http: Http;
    hostService:HostService;

    constructor( @Inject(Http) http: Http,@Inject(HostService) hostService: HostService) {
        this.http = http;
        this.hostService = hostService;
    }

    load(service: string) {
        let headers  = new Headers({'Access-Control-Allow-Origin':'*'});
        return this.http.get( this.hostService.getUrl(service));
    }
}