import { Component,Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'product-prod',
    templateUrl: 'dashboard.product.html',
    styleUrls:['dashboard.component.css']
    // inputs:[prods]
})
export class DashboardProductComponent {
    @Input('products') products: Array<any>;
    constructor() {
    }

}