import { Component, PipeTransform, Pipe } from '@angular/core';
// import { MdDialog, MdDialogRef, MdToolbar, MdCard, MaterialModule } from '@angular/material';
import { DashboardService, DashboardInterface } from './dashboard.service';
import { DashboardProductComponent } from './dashboard.products';
import { KeysPipe } from '../pipe';

@Component({
    moduleId: module.id,
    selector: 'material-app',
    viewProviders: [DashboardService],
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.css'],
    entryComponents: [ DashboardProductComponent ] 
})
export class DashboardComponent {
    logos: {} = {
        'CRITICAL': 'add_alert',
        'WARNING': 'error',
        'UNKNOWN': 'warning'
    };
    HOSTS_DOWN_SERVICES: {} = {
        'Product__IDM__Nebuchadnezzar__Alive__Status': 'Sites Down',
        'Administrative__Philips__Host__Reachability__Status': 'Hosts Down'
    }
    services: Array<any> = [];
    products: {};
    host_status: Array<any> = [];
    neb_status: Array<any> = [];
    pipes: [KeysPipe];
    dbService: DashboardService;

    constructor(dashboardService: DashboardService) {
        this.dbService = dashboardService
        this.load()
    }
    temp = {};
    load() {
        //Load Overall stats
        this.dbService.load('DASHBOARD')
            .map(response => response.json())
            .subscribe(
            result => this.temp = result,
            error => console.log('Error this time'),
            () => this.formatOverAllStats()
            )

        this.dbService.load('DB_HOSTS_DOWN')
            .map(response => response.json())
            .subscribe(
            result => this.temp = result,
            error => console.log('Error this time'),
            () => this.formatHostsNebStats(this.temp['result'])

            )

        this.dbService.load('DB_PRODUCTS')
            .map(response => response.json())
            .subscribe(
            result => this.temp = result,
            error => console.log('Error this time'),
            () => this.formatProductStats()
            )
    }
    formatHostsNebStats(result: Array<any>) {
        console.log(result);
        console.log(this.HOSTS_DOWN_SERVICES)
        for (let i in result) {
            var tt = this.HOSTS_DOWN_SERVICES[result[i]['service']];
            this.host_status.push({
                'count': result[i]['count'],
                'status': this.HOSTS_DOWN_SERVICES[result[i]['service']],
                'desc': tt.split(' ')[0],
                'icon': 'computer'
            })
        }
        console.log(this.host_status)
        //  this.host_status = [{
        //                     'count': this.temp['result'],
        //                     'status': 'host down',
        //                     'icon': 'computer'
        //                 }]
        //                  this.neb_status = [{
        //                     'count': this.temp['result'],
        //                     'status': 'host down',
        //                     'icon': 'computer'
        //                 }]
    }
    formatOverAllStats() {
        var services: Array<any> = [];
        if (this.temp) {
            var total_count = this.temp['total_count'];
            var result = this.temp['result'];
            for (let i in result) {
                var obj = result[i];
                var percent = Math.floor(obj.count / total_count * 100) + '%';
                services.push({
                    'percent': percent,
                    'count': obj.count,
                    'status': obj.status,
                    'icon': this.logos[obj.status]
                });
            }
            this.services = services;
        }
    }
    toggle() {
        alert("ss")
    }

    formatProductStats() {
        var services = {};
        if (this.temp) {
            var result = this.temp['result'];
            for (let i in result) {
                var obj = result[i];
                if (obj['product'].toString())
                    services[obj['product'].toString()] = {
                        'critical': obj['critical'],
                        'warning': obj['warning'],
                        'unknown': obj['unknown'],
                    }
            }
        }
        this.products = services;
    }
}
